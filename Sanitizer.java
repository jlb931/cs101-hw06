//A class to remove the Characters #, !, $, %, and & from a string.
public class Sanitizer
{
	private Sanitizer()
	{
		
	}
	
//	Uses the replaceAll method to return a sanitized version of the input string.
	public static String sanitize(String input)
	{
		String ret = input.replaceAll("[#!*$%&]", "");
		return ret;
	}
}
